#
# Guitar Class
# ************
#
# Created by Kevin Thomas 09/19/19.
# Modified by Kevin Thomas 09/25/19.
# CC BY 4.0
#
# Guitar class designed for a music retailer providing functionality to init brand, model and price and apply a standard discount for each class instantiation. 
#


import requests


class Guitar:
    """
    Guitar class with constructor attributes of brand, model and price
    where we init a constant discount of 10% and create three methods
    to which provides guitar instances a method to calculate a standard
    discount and then apply that standard discount in addition to
    a mock call to a URL to demonstrate mocking. Class can be overridden 
    with a child class for further customization. 
    """
    
    standard_discount = 0.10
    
    def __init__(self, brand=None, model=None, price=None):
        if brand is None:
            self._brand = 'Default Brand'
        else:
            self._brand = brand
        
        if model is None:
            self._model = 'Default Model'
        else:
            self._model = model
        
        if price is None:
            self._price = 1
        else:
            self._price = price

    @property
    def brand(self):
        return self._brand

    @brand.setter
    def brand(self, brand):
        if not brand:
            raise Exception('Brand name cannot be empty.')
        self._brand = brand

    @property
    def model(self):
        return self._model    

    @model.setter
    def model(self, model):
        if not model:
            raise Exception('Model name cannot be empty.')
        self._model = model

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, price):
        if not price:
            raise Exception('Price cannot be empty.')
        
        if price <= 0:
            raise ValueError('Price must be greater than zero.')
        self._price = price

    def calculate_discount(self):
        if self.price <= 0:
            raise ValueError('Price must be greater than zero.')
        discount = int(self.price * self.standard_discount)
        return discount

    def apply_discount(self):
        discount = self.calculate_discount()
        discount_price = int(self.price - discount)
        return discount_price
    
    def model_online_inventory_check(self):
        formatted_model_url = self.model.replace(' ', '_')        

        response = requests.get(f'https://guitar.com/{formatted_model_url}')
        
        if response.ok:
            return response.text
        else:
            return 'Bad Response!'

