# Guitar Class

Guitar class designed for a music retailer providing functionality to init brand, model and price and apply a standard discount for each class instantiation. 

## Usage

Run example guitar instance to demonstrate class usage.
```bash
python3 guitar_instance.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[CC BY 4.0](https://choosealicense.com/licenses/cc-by-4.0)