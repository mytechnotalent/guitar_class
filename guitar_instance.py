#
# Guitar Instance
# ***************
#
# Created by Kevin Thomas 09/19/19.
# Modified by Kevin Thomas 09/25/19.
# CC BY 4.0
#
# Guitar instance example to demonstrate new guitar object creation and usage.
#


import os
from guitar import Guitar


"""
Clear terminal.
"""
os.system('clear')


"""
Instantiate new guitar object and echo information to standard output.
"""
guitar_1 = Guitar('Fender', None, 1400)
print('{} {} - Price ${}'.format(guitar_1.brand, guitar_1.model, guitar_1.price))

"""
Calculate and apply discount and echo info to the standard output.
"""
discount_price = guitar_1.apply_discount()
print('{} {} - Discount Price ${}'.format(guitar_1.brand, guitar_1.model, discount_price))

