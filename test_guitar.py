#
# Test Guitar Class
# *****************
#
# Created by Kevin Thomas 09/19/19.
# Modified by Kevin Thomas 09/25/19.
# CC BY 4.0
#
# Python unittest to test guitar class functionality.
# Usage: python3 test_guitar.py
#


import unittest
from unittest.mock import patch
from guitar import Guitar


class TestGuitar(unittest.TestCase):
    """
    Test class which instantiates two guitar objects which test the
    discount calculation and discount apply methods.
    """
   
    def setUp(self):
        """
        Instantiate two guitar objects.
        """ 
        print('setUp')
        self.guitar_1 = Guitar('Fender', 'Strat', 1400)
        self.guitar_2 = Guitar('Gibson', 'Les Paul', 2000)
        self.guitar_3 = Guitar('Fender', 'Telecaster', 1100)
        self.guitar_4 = Guitar(None, None, 1800)   
 
    def tearDown(self):
        """
        Status echo to the standard output during runtime.
        """
        print('tearDown')

    def test_constructor(self):
        """
        Test constructor value assertions.
        """
        print('test_constructor')

        self.assertEqual(self.guitar_1.brand, 'Fender')
        self.assertEqual(self.guitar_1.model, 'Strat')
        self.assertEqual(self.guitar_3.price, 1100)
        with self.assertRaises(ValueError):
            self.guitar_3.price = -1
        self.assertEqual(self.guitar_4.brand, 'Default Brand')
        self.assertEqual(self.guitar_4.model, 'Default Model')
        self.assertEqual(self.guitar_4.price, 1800)

    def test_calculate_discount(self):
        """ 
        Test the assertion that the standard discount calculation 
        is accurate.
        """
        print('test_calculate_discount')

        self.assertEqual(self.guitar_1.standard_discount, 0.10)
        self.assertEqual(self.guitar_2.standard_discount, 0.10)
        with self.assertRaises(Exception):
            self.guitar_2.price = 0
        with self.assertRaises(ValueError):
            self.guitar_1.price = -1.5  
 
    def test_apply_discount(self):
        """ 
        Test the assertion that the discount was applied correctly.
        """
        print('test_apply_discount')
 
        self.assertEqual(self.guitar_1.apply_discount(), 1260.0)
        self.assertEqual(self.guitar_2.apply_discount(), 1800.0)
        
    def test_model_online_inventory_check(self):
        """
        Test the assertion that a model is available on the guitar.com
        website for purchase.  If it is no longer available the 
        link will be pulled down and empty.
        """
        print('test_model_online_inventory_check')

        with patch('guitar.requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Success'

            check_response = self.guitar_1.model_online_inventory_check()
            mocked_get.assert_called_with('https://guitar.com/Strat')
            self.assertEqual(check_response, 'Success')

            mocked_get.return_value.ok = False

            check_response = self.guitar_2.model_online_inventory_check()
            mocked_get.assert_called_with('https://guitar.com/Les_Paul')
            self.assertEqual(check_response, 'Bad Response!')
        

if __name__ == '__main__':
    unittest.main()

